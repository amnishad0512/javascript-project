function reset() {
  document.getElementById("doller").value = "";
  document.getElementById("inr").value = "";
  document.getElementById("error").innerText = "";
}
function converter() {
  var doller = Number(document.getElementById("doller").value);
  var inr = Number(document.getElementById("inr").value);
  if (doller <= 0) {
    document.getElementById("error").innerText =
      "Please Enter Positive Number ex. 1";
    document.getElementById("inr").value = " ";
  }
  if (doller >= 1) {
    var doll = 72.95;
    document.getElementById("inr").value = doller * doll;
    document.getElementById("error").innerText = "";
  }
  else {
    if (inr <= 0) {
      document.getElementById("error").innerText =
        "Please Enter Positive Number ex. 1";
      document.getElementById("doller").value = "";
    }
    else if (inr >= 1) {
      var ind = 0.0137;
      document.getElementById("doller").value = inr * ind;
      document.getElementById("error").innerText =
        "";
    }
  }
}