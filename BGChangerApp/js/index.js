$(document).click(function () {
    var red = Math.floor(Math.random() * 256);
    var green = Math.floor(Math.random() * 256);
    var blue = Math.floor(Math.random() * 256);
    var rgb = `RGB( ${red}, ${green}, ${blue} )`;
    $("p").text(`${rgb}`);
    $("#bg-color-change").css("background-color", `${rgb}`);
    $("button").css(
        { "background-color": `${rgb}` },
        { "background-color": `${rgb}` }
    );
});
