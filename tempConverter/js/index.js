$(document).on("click", "#reset", function () {
    $("#first-val").val("");
    $("#second-val").val("");
    $("#error").text("Temperature Converter");
});

$(document).on("click", "#Convert", function () {
    var firstVal = Number($("#first-val").val());
    var firstUnit = $("#first-unit").val();
    var secondVal = Number($("#second-val").val());
    var secondUnit = $("#second-unit").val();

    if (firstVal == "") {
        $("#error").text("Please Enter Value");
    } else if (firstUnit == "C" && secondUnit == "C") {
        $("#first-val").val("");
        $("#second-val").val("");
        $("#error").text("Please choose different Unit to Convert");
    } else if (firstUnit == "C" && secondUnit == "F") {
        $("#second-val").val((firstVal * 9) / 5 + 32);
        $("#error").text("");
    } else if (firstUnit == "C" && secondUnit == "K") {
        $("#second-val").val(firstVal + 273.15);
        $("#error").text("");
    } else if (firstUnit == "C" && secondUnit == "R") {
        $("#second-val").val(((firstVal + 273.15) * 9) / 5);
        $("#error").text("");
    } else if (firstUnit == "C" && secondUnit == "De") {
        $("#second-val").val(((100 - firstVal) * 3) / 2);
        $("#error").text("");
    } else if (firstUnit == "C" && secondUnit == "N") {
        $("#second-val").val((firstVal * 33) / 100);
        $("#error").text("");
    } else if (firstUnit == "C" && secondUnit == "Re") {
        $("#second-val").val((firstVal * 4) / 5);
        $("#error").text("");
    } else if (firstUnit == "C" && secondUnit == "Ro") {
        $("#second-val").val((firstVal * 21) / 40 + 7.5);
        $("#error").text("");
    } else if (firstUnit == "De" && secondUnit == "C") {
        $("#second-val").val(100 - (firstVal * 2) / 3);
        $("#error").text("");
    } else if (firstUnit == "De" && secondUnit == "F") {
        $("#second-val").val(212 - (firstVal * 6) / 5);
        $("#error").text("");
    } else if (firstUnit == "De" && secondUnit == "K") {
        $("#second-val").val(373.15 - (firstVal * 2) / 3);
        $("#error").text("");
    } else if (firstUnit == "De" && secondUnit == "R") {
        $("#second-val").val(671.67 - (firstVal * 6) / 5);
        $("#error").text("");
    } else if (firstUnit == "De" && secondUnit == "N") {
        $("#second-val").val(33 - (firstVal * 11) / 50);
        $("#error").text("");
    } else if (firstUnit == "De" && secondUnit == "Re") {
        $("#second-val").val(80 - (firstVal * 8) / 15);
        $("#error").text("");
    } else if (firstUnit == "De" && secondUnit == "Ro") {
        $("#second-val").val(60 - (firstVal * 7) / 20);
        $("#error").text("");
    } else if (firstUnit == "F" && secondUnit == "C") {
        $("#second-val").val(((firstVal - 32) * 5) / 9);
        $("#error").text("");
    } else if (firstUnit == "F" && secondUnit == "K") {
        $("#second-val").val(((firstVal + 459.67) * 5) / 9);
        $("#error").text("");
    } else if (firstUnit == "F" && secondUnit == "R") {
        $("#second-val").val(firstVal + 459.67);
        $("#error").text("");
    } else if (firstUnit == "F" && secondUnit == "De") {
        $("#second-val").val(((212 - firstVal) * 5) / 6);
        $("#error").text("");
    } else if (firstUnit == "F" && secondUnit == "N") {
        $("#second-val").val(((firstVal - 32) * 11) / 60);
        $("#error").text("");
    } else if (firstUnit == "F" && secondUnit == "Re") {
        $("#second-val").val(((firstVal - 32) * 4) / 9);
        $("#error").text("");
    } else if (firstUnit == "F" && secondUnit == "Ro") {
        $("#second-val").val(((firstVal - 32) * 7) / 24 + 7.5);
        $("#error").text("");
    } else if (firstUnit == "K" && secondUnit == "C") {
        $("#second-val").val(firstVal - 273.15);
        $("#error").text("");
    } else if (firstUnit == "K" && secondUnit == "F") {
        $("#second-val").val((firstVal * 9) / 5 - 459.67);
        $("#error").text("");
    } else if (firstUnit == "K" && secondUnit == "R") {
        $("#second-val").val((firstVal * 9) / 5);
        $("#error").text("");
    } else if (firstUnit == "K" && secondUnit == "De") {
        $("#second-val").val(((373.15 - firstVal) * 3) / 2);
        $("#error").text("");
    } else if (firstUnit == "K" && secondUnit == "N") {
        $("#second-val").val(((firstVal - 273.15) * 33) / 100);
        $("#error").text("");
    } else if (firstUnit == "K" && secondUnit == "Re") {
        $("#second-val").val(((firstVal - 273.15) * 4) / 5);
        $("#error").text("");
    } else if (firstUnit == "K" && secondUnit == "Ro") {
        $("#second-val").val(((firstVal - 273.15) * 21) / 40 + 7.5);
        $("#error").text("");
    } else if (firstUnit == "N" && secondUnit == "C") {
        $("#second-val").val((firstVal * 100) / 33);
        $("#error").text("");
    } else if (firstUnit == "N" && secondUnit == "F") {
        $("#second-val").val((firstVal * 60) / 11 + 32);
        $("#error").text("");
    } else if (firstUnit == "N" && secondUnit == "K") {
        $("#second-val").val((firstVal * 100) / 33 + 273.15);
        $("#error").text("");
    } else if (firstUnit == "N" && secondUnit == "R") {
        $("#second-val").val((firstVal * 60) / 11 + 491.67);
        $("#error").text("");
    } else if (firstUnit == "N" && secondUnit == "De") {
        $("#second-val").val(((33 - firstVal) * 50) / 11);
        $("#error").text("");
    } else if (firstUnit == "N" && secondUnit == "Re") {
        $("#second-val").val((firstVal * 80) / 33);
        $("#error").text("");
    } else if (firstUnit == "N" && secondUnit == "Ro") {
        $("#second-val").val((firstVal * 35) / 22 + 7.5);
        $("#error").text("");
    } else if (firstUnit == "R" && secondUnit == "C") {
        $("#second-val").val(((firstVal - 491.67) * 5) / 9);
        $("#error").text("");
    } else if (firstUnit == "R" && secondUnit == "F") {
        $("#second-val").val(firstVal - 459.67);
        $("#error").text("");
    } else if (firstUnit == "R" && secondUnit == "K") {
        $("#second-val").val((firstVal * 5) / 9);
        $("#error").text("");
    } else if (firstUnit == "R" && secondUnit == "De") {
        $("#second-val").val(((671.67 - firstVal) * 5) / 6);
        $("#error").text("");
    } else if (firstUnit == "R" && secondUnit == "N") {
        $("#second-val").val(((firstVal - 491.67) * 11) / 60);
        $("#error").text("");
    } else if (firstUnit == "R" && secondUnit == "Re") {
        $("#second-val").val(((firstVal - 491.67) * 4) / 9);
        $("#error").text("");
    } else if (firstUnit == "R" && secondUnit == "Ro") {
        $("#second-val").val(((firstVal - 491.67) * 7) / 24 + 7.5);
        $("#error").text("");
    } else if (firstUnit == "Re" && secondUnit == "C") {
        $("#second-val").val((firstVal * 5) / 4);
        $("#error").text("");
    } else if (firstUnit == "Re" && secondUnit == "F") {
        $("#second-val").val((firstVal * 9) / 4 + 32);
        $("#error").text("");
    } else if (firstUnit == "Re" && secondUnit == "K") {
        $("#second-val").val((firstVal * 5) / 4 + 273.15);
        $("#error").text("");
    } else if (firstUnit == "Re" && secondUnit == "R") {
        $("#second-val").val((firstVal * 9) / 4 + 491.67);
        $("#error").text("");
    } else if (firstUnit == "Re" && secondUnit == "De") {
        $("#second-val").val(((80 - firstVal) * 15) / 8);
        $("#error").text("");
    } else if (firstUnit == "Re" && secondUnit == "N") {
        $("#second-val").val((firstVal * 33) / 80);
        $("#error").text("");
    } else if (firstUnit == "Re" && secondUnit == "Ro") {
        $("#second-val").val((firstVal * 21) / 32 + 7.5);
        $("#error").text("");
    } else if (firstUnit == "Ro" && secondUnit == "C") {
        $("#second-val").val(((firstVal - 7.5) * 40) / 21);
        $("#error").text("");
    } else if (firstUnit == "Ro" && secondUnit == "F") {
        $("#second-val").val(((firstVal - 7.5) * 24) / 7 + 32);
        $("#error").text("");
    } else if (firstUnit == "Ro" && secondUnit == "K") {
        $("#second-val").val(((firstVal - 7.5) * 40) / 21 + 273.15);
        $("#error").text("");
    } else if (firstUnit == "Ro" && secondUnit == "R") {
        $("#second-val").val(((firstVal - 7.5) * 24) / 7 + 491.67);
        $("#error").text("");
    } else if (firstUnit == "Ro" && secondUnit == "D") {
        $("#second-val").val(((60 - firstVal) * 20) / 7);
        $("#error").text("");
    } else if (firstUnit == "Ro" && secondUnit == "N") {
        $("#second-val").val((firstVal - 7.5) * 22.35);
        $("#error").text("");
    } else if (firstUnit == "Ro" && secondUnit == "Re") {
        $("#second-val").val(((firstVal - 7.5) * 32) / 21);
        $("#error").text("");
    } else if (firstUnit == "F" && secondUnit == "F") {
        $("#first-val").val("");
        $("#second-val").val("");
        $("#error").text("Please choose different Unit to Convert");
    } else if (firstUnit == "K" && secondUnit == "K") {
        $("#first-val").val("");
        $("#second-val").val("");
        $("#error").text("Please choose different Unit to Convert");
    } else if (firstUnit == "R" && secondUnit == "R") {
        $("#first-val").val("");
        $("#second-val").val("");
        $("#error").text("Please choose different Unit to Convert");
    } else if (firstUnit == "De" && secondUnit == "De") {
        $("#first-val").val("");
        $("#second-val").val("");
        $("#error").text("Please choose different Unit to Convert");
    } else if (firstUnit == "N" && secondUnit == "N") {
        $("#first-val").val("");
        $("#second-val").val("");
        $("#error").text("Please choose different Unit to Convert");
    } else if (firstUnit == "Re" && secondUnit == "Re") {
        $("#first-val").val("");
        $("#second-val").val("");
        $("#error").text("Please choose different Unit to Convert");
    } else if (firstUnit == "Ro" && secondUnit == "Ro") {
        $("#first-val").val("");
        $("#second-val").val("");
        $("#error").text("Please choose different Unit to Convert");
    }
});