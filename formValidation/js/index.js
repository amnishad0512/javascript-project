function sampleFunction() {
    location.reload();
}
function formValidation() {
    //<---------------Company-Name---------------->
    var name = document.getElementById("name").value;
    if (name == "") {
        var eName = (document.getElementById("eName").innerHTML =
            "Please enter your company name");
        return false;
    }
    else if (!name.match(/^[a-zA-Z" "]+$/)) {
        var eName = (document.getElementById("eName").innerHTML =
            "Only Character are allow");
        return false;
    } else if (name.length <= 14) {
        var eName = (document.getElementById("eName").innerHTML =
            "Minimum text length should be 15 characters");
        return false;
    } else if (name.length >= 26) {
        var eName = (document.getElementById("eName").innerHTML =
            "Maximum text length should be 25 characters");
        return false;
    } else {
        var eName = (document.getElementById("eName").innerHTML = "")
            ;
    }
    //<---------------Email-ID---------------->

    var email = document.getElementById("email").value;
    if (email == "") {
        var eEmail = (document.getElementById("eEmail").innerHTML =
            "Please enter email id");
        return false;
    } else if (email.indexOf('@') === 0) {
        var eEmail = (document.getElementById("eEmail").innerHTML =
            "@ position is invalid");
        return false;
    }
    else if (email.length >= 31) {
        var eEmail = (document.getElementById("eEmail").innerHTML =
            "Maximum text length should be 25 characters");
        return false;
    } else {
        var eEmail = (document.getElementById("eEmail").innerHTML = "");
    }

    //<---------------Password---------------->

    var pass = document.getElementById("pass").value;
    if (pass == "") {
        var ePass = (document.getElementById("ePass").innerHTML =
            "Please enter your password");
        return false;
    } else if (pass.length !== 6) {
        var ePass = (document.getElementById("ePass").innerHTML =
            "Password length exactly 6");
        return false;
    } else if (!pass.match(/[A-Z]/g)) {
        var ePass = (document.getElementById("ePass").innerHTML =
            "Should be at least 1 uppercase Character");
        return false;
    } else if (!pass.match(/[a-z]/g)) {
        var ePass = (document.getElementById("ePass").innerHTML =
            "Should be at least 1 lowercase Character");
        return false;
    } else if (!pass.match(/[0-9]/g)) {
        var ePass = (document.getElementById("ePass").innerHTML =
            "Should be at least 1 digit");
        return false;
    } else if (!pass.match(/[^a-zA-Z\d]/g)) {
        var ePass = (document.getElementById("ePass").innerHTML =
            "Should be at least 1 special character");
        return false;
    } else {
        var ePass = (document.getElementById("ePass").innerHTML = "");
    }

    //<---------------Confirm-Password---------------->
    var cPass = document.getElementById("cPass").value;
    if (cPass == "") {
        var eCPass = (document.getElementById("eCPass").innerHTML =
            "Please enter your confirm password");
        return false;
    } else if (cPass !== pass) {
        var eCPass = (document.getElementById("eCPass").innerHTML =
            "Password did not match");
        return false;
    } else {
        var eCPass = (document.getElementById("ePass").innerHTML = "");
        console.log(cPass);
        alert("all input are valid")
    }
}