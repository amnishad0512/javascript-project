$(document).on("click", "#submit", function () {
  var name = $("#fname").val();
  var message = $("#message").val();
  var email = $("#email").val();
  if (name == "") {
    $("#error").css({
      "font-size": "20px",
      "margin-left": "50px",
      "background-color": "black",
      padding: "5px 12px",
      width: "597px",
    });
    $("#error").text("Please Enter Your Full Name");
    return false;
  } else if (email == "") {
    $("#email").css({
      "font-size": "20px",
      "margin-left": "50px",
      "background-color": "black",
      padding: "5px 12px",
      width: "597px",
    });
    $("#error").text("Please Enter Your Email");
    return false;
  } else if (message == "") {
    $("#error").css({
      "font-size": "20px",
      "margin-left": "50px",
      "background-color": "black",
      padding: "5px 12px",
      width: "597px",
    });
    $("#error").text("Please Enter Your Message");
    return false;
  }
});