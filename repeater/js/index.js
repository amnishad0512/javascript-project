var i = 2;
$(document).on("click", "#add-more", function () {
    var ele = `<table id ="div${i}">
      <tr>
        <td>${i}</td>
        <td><input type="text" id="passender${i}" /></td>
        <td><input type="number" /></td>
        <td>
          <select name="gender" id="">
            <option disabled selected></option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
            <option value="Other">Other</option>
          </select>
        </td>
        <td>
          <select name="berth" id="">
            <option>No Preference</option>
            <option value="Male">Lower</option>
            <option value="Female">Middle</option>
            <option value="Other">Aisle</option>
          </select>
        </td>
        <td><input type="checkbox" /></td>
        <td><button id="${i}" data-btn-id=${i} class = "remove-btn">Delete</button></td>
      </tr>
    </table>`;
    $(".repeater").append(ele);
    i++;
});
$(document).on("click", ".remove-btn", function () {
    var id = $(this).data("btn-id");
    $(`#div${id}`).remove();
});
