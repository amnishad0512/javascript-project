function reset() {
  document.getElementById("input").placeholder = 0;
  document.getElementById("error").innerText = "";
}
function max() {
  var a = document.getElementById("input").placeholder;
  parseInt(a);
  a++;
  document.getElementById("input").placeholder = a++;
  document.getElementById("error").innerText = "";
}
function min() {
  var a = document.getElementById("input").placeholder;
  parseInt(a);
  if (a <= 0) {

    document.getElementById("error").innerText =
      `Can not go less than 0`;
  } else {
    a--;
    document.getElementById("input").placeholder = a--;
    document.getElementById("error").innerText = "";
  }
}