function reset() {
    calc.display.value = "";
}
var buttons = document.querySelectorAll('button');
var primaryOperand = '';
var secondaryOperand = '';
var operand = '';
var operand2 = '';
var savedOperator = '';
var results = '';
var operator = '';
function calculateNumber(primaryOperand, operator, secondaryOperand) {
    results = eval(primaryOperand + operator + secondaryOperand);
    return results;
}

function saveOperator(operator) {
    savedOperator = operator;
    return savedOperator;
}

function getPrimaryOperand() {
    buttons.forEach(function (button) {
        button.addEventListener('click', function (e) {

            operand = e.target.textContent

            if (operand !== "0" && operand !== "+" && operand !== "-" && operand !== "X" && operand !== "÷" && operand !== "=" && operand !== "C") {
                primaryOperand += e.target.textContent;
                console.log(primaryOperand);
            } else if (operand === "+" || operand === "-" || operand === "X" || operand === "÷" || operand === "=" || operand !== "C") {
                operator = operand;
                getSecondaryOperand(operator);
            }
        })
    })
}
function getSecondaryOperand(operator) {
    buttons.forEach(function (button) {
        button.addEventListener('click', function (e) {

            operand2 = e.target.textContent

            if (operand2 !== "0" && operand2 !== "+" && operand2 !== "-" && operand2 !== "X" && operand2 !== "÷" && operand2 !== "=" && operand2 !== "C") {
                secondaryOperand += e.target.textContent;
                console.log(secondaryOperand);
            } else if (operand === "=") {

                calculateNumber(primaryOperand, operator, secondaryOperand);
            }
        })
    })
}
getPrimaryOperand();