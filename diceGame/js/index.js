$(document).on("click", "#roll-btn", function () {
    var red = Math.floor(Math.random() * 6);
    var green = Math.floor(Math.random() * 6);
    var blue = Math.floor(Math.random() * 6);
    var yellow = Math.floor(Math.random() * 6);
    $(".red").css("background-image", `url(img/${red}.png)`);
    $(".green").css("background-image", `url(img/${green}.png)`);
    $(".blue").css("background-image", `url(img/${blue}.png)`);
    $(".yellow").css("background-image", `url(img/${yellow}.png)`);
});